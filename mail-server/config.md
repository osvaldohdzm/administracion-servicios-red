# Servidor de correo

## Archivos necesarios

### Repositorios de Jessie

Editar archivo de repositorios:
```
sudo nano /etc/apt/sources.list 
```

```
deb http://ftp.debian.org/debian/ jessie main contrib non-free
deb-src http://ftp.debian.org/debian/ jessie main contrib non-free
deb http://security.debian.org/ jessie/updates main contrib non-free
deb-src http://security.debian.org/ jessie/updates main contrib non-free
```

# Configurar hostname

Cambiar el hostname:
```bash
sudo nano /etc/hosts
```

Agregar o modificar la siguiente linea del archivo el nombrede host:

```
127.0.1.1       mail.escom.edu
```
```bash
sudo nano /etc/hostname
```

Agregar o modificar la siguiente linea del archivo:

```
mail.escom.edu
```
```bash
sudo hostnamectl set-hostname mail.escom.edu
```
```bash
sudo nano /etc/mailname
```

## Instalación 


# Instalar apache
sudo apt-get install apache2
# Cambiar borrar la parte de http en la ruta var/www
sudo nano /etc/apache2/sites-available/000-default.conf 



# Instalación y configuración de POSTFIX

Instalar postfix

```
sudo apt-get install postfix
```
Para crear un directorio de correo de un usuario: sudo maildirmake /home/nombre_usuario/Maildir 
Para crear el directorio de correo estandar: 
```sudo maildirmake /home/Maildir
```
escom.name es el FQN

Editar el archivo de configuracion de postfix
```
inet_protocols = ipv4
home_mailbox = Maildir/
myhostname = thinkpadt530A01.debian.name
mydomain = debian.name
myorigin = $mydomain
mydestination = $myhostname, $mydomain,  localhost.debian.name, localhost
```
```
sudo nano /etc/postfix/main.cf
```
# Reiniciar postifx
```
sudo /etc/init.d/postfix restart
```
# (alternativa) 
```
sudo systemctl restart postfix.service 
sudo systemctl status postfix.service 
(alternativa) sudo service postfix reload
```

#----------------------------------------

# (Opcional) Instalar courier  IMAP POP3 Server
sudo apt-get install courier-pop courier-imap



#  Instalar dovecot IMAP POP3 Server
sudo apt-get install dovecot-imapd dovecot-pop3d
# #disable_plaintext_auth = yes    ->    disable_plaintext_auth = no
# auth_mechanisms = plain login
# El directorio de configuración tiene varios archivos 
ls /etc/dovecot/conf.d/
# Colocar los archivos de correo en mail_location = maildir:~/Maildir
sudo nano /etc/dovecot/conf.d/10-mail.conf 

# Reiniciar el servicio
sudo service dovecot restart
# (alternativa) 
```
sudo systemctl restart dovecot.service 
sudo systemctl status dovecot.service 
```
#-------------------------------------------------------

# Instalar squirrel 
sudo apt-get install squirrelmail
# Configurar 2 > nombre dominio
# Configurar 
# Command >> D
# (Opcional) Command >> Courier
# Probar con dovecot:	Command >> Dovecot
# (Opcional )Command >> 11 >> “Allow server-side sorting” True
# El nombre del servidor debe ser el mismo del hostnamectl
sudo squirrelmail-configure
# 
sudo ln -s /usr/share/squirrelmail/ /var/www/webmail
sudo /etc/init.d/apache2 restart
# (alter)
sudo systemctl restart apache2.service 


# Instalar comando mail
sudo apt-get install mailutils

# Enviar correo
mail osvaldo

# Ver log 
sudo tail -n 10 /var/log/mail.info
sudo tail -n 10 /var/log/mail.err
sudo tail -n 10 /var/log/mail.log



# Checkservices  port
sudo netstat -tlpn
sudo systemctl status dovecot.service

# Checar cola de correo
mailq

Borrar cola:
```
sudo postsuper -d ALL
```
Reintentar
sudo postqueue -f


# Careptade  correos ver correos
cat /var/mail/usuario  
 



## Backup configuration files

``` bash
sudo mkdir -p /home/$LOGNAME/Escritorio/administracion-servicios-red/mail-server/configuration-files/
```
``` bash
sudo cp /etc/postfix/main.cf /home/$LOGNAME/Escritorio/administracion-servicios-red/mail-server/configuration-files/

sudo cp /etc/postfix/master.cf /home/$LOGNAME/Escritorio/administracion-servicios-red/mail-server/configuration-files/

sudo cp /etc/dovecot/conf.d/10-mail.conf /home/$LOGNAME/Escritorio/administracion-servicios-red/mail-server/configuration-files/

sudo cp /etc/dovecot/conf.d/90-quota.conf /home/$LOGNAME/Escritorio/administracion-servicios-red/mail-server/configuration-files/

sudo cp /etc/aliases /home/$LOGNAME/Escritorio/administracion-servicios-red/mail-server/configuration-files/

sudo cp /etc/dovecot/ /home/$LOGNAME/Escritorio/administracion-servicios-red/mail-server/configuration-files/

sudo cp /etc/postfix/sasl_passwd /home/$LOGNAME/Escritorio/administracion-servicios-red/mail-server/configuration-files/

sudo cp /etc/postfix/sender_access /home/$LOGNAME/Escritorio/administracion-servicios-red/mail-server/configuration-files/

sudo cp /etc/postfix/client_access /home/$LOGNAME/Escritorio/administracion-servicios-red/mail-server/configuration-files/

sudo cp /etc/default/spamassassin /home/$LOGNAME/Escritorio/administracion-servicios-red/mail-server/configuration-files/

sudo cp /etc/spamassassin/local.cf /home/$LOGNAME/Escritorio/administracion-servicios-red/mail-server/configuration-files/
```
Reverso:
``` bash
sudo cp /home/$LOGNAME/Escritorio/administracion-servicios-red/mail-server/configuration-files/main.cf /etc/postfix/ 

sudo cp /home/$LOGNAME/Escritorio/administracion-servicios-red/mail-server/configuration-files/10-mail.conf /etc/dovecot/conf.d/

sudo cp /home/$LOGNAME/Escritorio/administracion-servicios-red/mail-server/configuration-files/90-quota.conf /etc/dovecot/conf.d/

sudo cp /home/$LOGNAME/Escritorio/administracion-servicios-red/mail-server/configuration-files/aliases /etc/

sudo cp /home/$LOGNAME/Escritorio/administracion-servicios-red/mail-server/configuration-files/sasl_passwd /etc/postfix/

sudo cp /home/$LOGNAME/Escritorio/administracion-servicios-red/mail-server/configuration-files/sender_access /etc/postfix/

sudo cp /home/$LOGNAME/Escritorio/administracion-servicios-red/mail-server/configuration-files/client_access /etc/postfix/

sudo cp /home/$LOGNAME/Escritorio/administracion-servicios-red/mail-server/configuration-files/spamassassin /etc/default/

sudo cp /home/$LOGNAME/Escritorio/administracion-servicios-red/mail-server/configuration-files/local.cf /etc/spamassassin/
```
```bash
sudo systemctl restart postfix
sudo systemctl status postfix 
```
```bash
sudo chmod 777 -R /home/usuario/Escritorio/administracion-servicios-red/
```

## Desinstalacion

``` bash
sudo apt-get remove nginx nginx-common
sudo apt-get remove --purge postfix 
sudo apt-get remove --purge apache2 postfix dovecot-imapd dovecot-pop3d squirrelmail -y
sudo apt-get autoremov
```

## Configure gmail as Relay Gmail
```bash
sudo nano /etc/postfix/sasl_passwd
```
[smtp.gmail.com]:587    user@gmail.com:password
```
sudo nano /etc/postfix/main.cf
```
relayhost = [smtp.gmail.com]:587
smtp_use_tls = yes
smtp_sasl_auth_enable = yes
smtp_sasl_security_options = noanonymous
smtp_sasl_tls_security_options = noanonymous
smtp_sasl_password_maps = hash:/etc/postfix/sasl_passwd
smtp_tls_CAfile = /etc/ssl/certs/ca-certificates.crt
```


sudo postmap /etc/postfix/sasl_passwd

sudo systemctl restart postfix
sudo systemctl status postfix

# Luego hay que habilitatr secure apps https://myaccount.google.com/lesssecureapps?pli=1
```
```


Cuota de buzon y adjutos Dovecot:
```
sudo nano /etc/dovecot/conf.d/90-quota.conf 
sudo systemctl restart dovecot.service 


sudo nano /etc/postfix/main.cf
Agregar:

message_size_limit = 20480000
mailbox_size_limit = 0



```
Verificar
```
sudo dovecot -n
```



### Notas



# Crear usuarios
sudo useradd osvaldo
sudo passwd osvaldo
sudo mkhomedir_helper osvaldo

# checar usuario activo
whoami

# cambiar usuario
su - user2

# Crear directorios de correo, $LOGNAME es el usuario
sudo maildirmake /home/$LOGNAME/Maildir

#Cambiar poriertario
sudo chown usuario Maildir/
sudo chgrp usuario Maildir/



# Montorozar servicio actibvo
sudo apt-get install rcconf
sudo  rcconf


# Crear lista de usuarios demostración

sudo touch users-list.txt
sudo nano users-list.txt
```
alumno01:x:1011:1011::/home/alumno01:/bin/bash
alumno02:x:1012:1012::/home/alumno02:/bin/bash
alumno03:x:1013:1013::/home/alumno03:/bin/bash
alumno04:x:1014:1014::/home/alumno04:/bin/bash
alumno05:x:1015:1015::/home/alumno05:/bin/bash
alumno06:x:1016:1016::/home/alumno06:/bin/bash
alumno07:x:1017:1017::/home/alumno07:/bin/bash
alumno08:x:1018:1018::/home/alumno08:/bin/bash
alumno09:x:1019:1019::/home/alumno09:/bin/bash
alumno10:x:1020:1020::/home/alumno10:/bin/bash
alumno11:x:1021:1021::/home/alumno11:/bin/bash
alumno12:x:1022:1022::/home/alumno12:/bin/bash
alumno13:x:1023:1023::/home/alumno13:/bin/bash
alumno14:x:1024:1024::/home/alumno14:/bin/bash
alumno15:x:1025:1025::/home/alumno15:/bin/bash
alumno16:x:1026:1026::/home/alumno16:/bin/bash
alumno17:x:1027:1027::/home/alumno17:/bin/bash
alumno18:x:1028:1028::/home/alumno18:/bin/bash
alumno19:x:1029:1029::/home/alumno19:/bin/bash
alumno20:x:1030:1030::/home/alumno20:/bin/bash
alumno21:x:1031:1031::/home/alumno21:/bin/bash
alumno22:x:1032:1032::/home/alumno22:/bin/bash
alumno23:x:1033:1033::/home/alumno23:/bin/bash
alumno24:x:1034:1034::/home/alumno24:/bin/bash
alumno25:x:1035:1035::/home/alumno25:/bin/bash
alumno26:x:1036:1036::/home/alumno26:/bin/bash
alumno27:x:1037:1037::/home/alumno27:/bin/bash
alumno28:x:1038:1038::/home/alumno28:/bin/bash
alumno29:x:1039:1039::/home/alumno29:/bin/bash
alumno30:x:1040:1040::/home/alumno30:/bin/bash
```



sudo newusers users-list.txt 
sudo rm users-list.txt

less /etc/passwd 


Borrar usuario
sudo deluser alumno01
sudo deluser --remove-home alumno01
sudo deluser --remove-home alumno02
sudo deluser --remove-home alumno03
sudo deluser --remove-home alumno04
sudo deluser --remove-home alumno05
sudo deluser --remove-home alumno06
sudo deluser --remove-home alumno07
sudo deluser --remove-home alumno08
sudo deluser --remove-home alumno09
sudo deluser --remove-home alumno10
sudo deluser --remove-home alumno11
sudo deluser --remove-home alumno12
sudo deluser --remove-home alumno13
sudo deluser --remove-home alumno14
sudo deluser --remove-home alumno15
sudo deluser --remove-home alumno16
sudo deluser --remove-home alumno17
sudo deluser --remove-home alumno18
sudo deluser --remove-home alumno19
sudo deluser --remove-home alumno20
sudo deluser --remove-home alumno21
sudo deluser --remove-home alumno22
sudo deluser --remove-home alumno23
sudo deluser --remove-home alumno24
sudo deluser --remove-home alumno25
sudo deluser --remove-home alumno26
sudo deluser --remove-home alumno27
sudo deluser --remove-home alumno28
sudo deluser --remove-home alumno29
sudo deluser --remove-home alumno30

# Configurar alias (listas de correo)
```
sudo nano /etc/aliases

sudo newaliases
sudo systemctl restart postfix



# Configurar lista negra y blanca

We will define the whitelist or blacklist with and OK or REJECT, followed by an optional answer text.
OK is allowed
REJECT is block

It is important that check_client_access and check_sender_access are defined as some of the first in smtpd_recipient_restrictions.
We do this, so the e-mail is not caught by some of the other filters we have set.

Lista de envio correos bloqueados para enviar direcciones para aceptar comandos:



```
sudo nano /etc/postfix/sender_access
```
```
# Restricts sender addresses this system accepts in MAIL FROM commands.
alumno30@escom.edu REJECT
```
```bash
sudo nano /etc/postfix/main.cf
```
```
smtpd_recipient_restrictions = check_sender_access hash:/etc/postfix/sender_access
```
```bash
sudo postmap hash:/etc/postfix/sender_access
```

Lista de clientes:

```
sudo nano /etc/postfix/client_access
```
```
# Restricts which clients this system accepts SMTP connections from.

escom.edu                 OK
321.987.654.321           REJECT
```
```
sudo postmap /etc/postfix/client_access
```

https://www.serveradminblog.com/2010/03/how-to-whitelist-hosts-or-ip-addresses-in-postfix/
https://linuxlasse.net/linux/howtos/Blacklist_and_Whitelist_with_Postfix

# PGP GPG


open pgp addon thunderbird

sudo nano /etc/dovecot/conf.d/15-lda.conf 

protocol lda {
    mail_plugins = $mail_plugins sieve
}
protocol lmtp {
    mail_plugins = $mail_plugins sieve
}


# Respaldos de usuarios y de correos

sudo mkdir /var/mail-backup-1110201901

sudo cp -r /var/mail/* /var/mail-backup-1110201901

(Prueba) 

sudo rm -r /var/mail/*

Restaurar
````bash
sudo cp -r /var/mail-backup-1110201901/* /var/mail/
sudo chmod 777 /var/mail/*
```

# SPAM
(prueba) sudo apt install sendmail sendmail-cf m4

Configurar:

```
sudo sendmailconfig
```
```bash
apt-get update
sudo apt-get install spamc spamassassin
sudo  groupadd -g 5555 spamd
```

```bash
sudo useradd spamd -s /bin/false -d /var/log/spamassasin
mkdir -p /var/log/spamassasin
mkdir -p /usr/local/spamassassin/log
sudo chown spamd:spamd -R /usr/local/spamassassin
sudo chown spamd:spamd -R /var/log/spamassasin
```
```bash
sudo nano /etc/default/spamassassin 
```
```
ENABLED=1
```
```bash
sudo nano /etc/spamassassin/local.cf
```
```
rewrite_header Subject *****SPAM*****
required_score 3.0
report_safe 0
use_bayes 1
bayes_auto_learn 1
```

sudo systemctl enable spamassassin.service

sudo sa-update 



sudo nano /etc/postfix/master.cf

Cambiar linea por:

```
smtp      inet  n       -       y       -       -       smtpd -o content_filter=spamassassin
```
(Prueba)
```
spamassassin unix - n n - - pipe
  user=spamd argv=/usr/bin/spamc -f -e /usr/sbin/
```
o bien:

```
spamassassin unix - n n - - pipe flags=R user=spamd argv=/usr/bin/spamc -e /usr/sbin/sendmail -oi -f ${sender} ${recipient}
```

sudo service spamassassin restart

sudo service postfix restart
mailq
sudo netstat -tnulp | grep spam


https://www.tecmint.com/integrate-clamav-and-spamassassin-to-protect-postfix-mails-from-viruses/

# Correo de prueba

mail -s "Test spam mail (GTUBE)" osvaldo@escom.edu <<< 'XJS*C4JDBQADN1.NSBN3*2IDNEN*GTUBE-STANDARD-ANTI-UBE-TEST-EMAIL*C.34X'

mail -s "Test spam mail (GTUBE)" osvaldo@escom.edu <<< 'This is the GTUBE, the
        Generic
        Test for
        Unsolicited
        Bulk
        Email

If your spam filter supports it, the GTUBE provides a test by which you
can verify that the filter is installed correctly and is detecting incoming
spam. You can send yourself a test mail containing the following string of
characters (in upper case and with no white spaces and line breaks):

XJS*C4JDBQADN1.NSBN3*2IDNEN*GTUBE-STANDARD-ANTI-UBE-TEST-EMAIL*C.34X

You should send this test mail from an account outside of your network.
'

mail -s "Test spam mail (GTUBE)" alumno01@escom.edu <<< 'XJS*C4JDBQADN1.NSBN3*2IDNEN*GTUBE-STANDARD-ANTI-UBE-TEST-EMAIL*C.34X'

# Instalar thunderbird
sudo apt-get install thunderbird

# Antivirus
```bash
sudo apt-get install clamav-daemon clamsmtp 
```
```bash
sudo adduser clamsmtp clamav
```
```bash
sudo nano  /etc/clamsmtpd.conf
```
line 27: uncomment
Header: X-AV-Checked: ClamAV using ClamSMTP

line 45: change
User: clamav
```bash
sudo chown -R clamav. /var/spool/clamsmtp

```
```bash
sudo chown -R clamav. /var/run/clamsmtp
sudo chmod 777 -R /var/run/clamsmtp
```
```bash
sudo nano /etc/postfix/main.cf
```
# add to the end
```bash
content_filter = scan:127.0.0.1:10025
```
```bash
sudo nano /etc/postfix/master.cf
```
```
127.0.0.1:10026 inet  n -       n       -       16      smtpd
        -o content_filter=
        -o receive_override_options=no_unknown_recipient_checks,no_header_bo$
        -o smtpd_helo_restrictions=
        -o smtpd_client_restrictions=
        -o smtpd_sender_restrictions=
        -o smtpd_recipient_restrictions=permit_mynetworks,reject
        -o mynetworks_style=host
        -o smtpd_authorized_xforward_hosts=127.0.0.0/8
```
```bash
sudo systemctl restart clamav-daemon clamsmtp postfix
```

Actualizar base datos de antivuros:
```bash
sudo freshclam 
```

# Desisntalar
```bash
sudo apt-get remove --purge clamav-daemon clamsmtp 
sudo apt autoremove
```
https://www.linux.com/tutorials/using-clamav-kill-viruses-postfix/

https://www.virtualhelp.me/linux/777-postfix-and-clam-av


https://www.server-world.info/en/note?os=Debian_7.0&p=mail&f=6

```bash
 sudo chmod u=rwx,g=rwx,o=rwx /var/spool/clamsmtp/
```
See log file
```bash
tail /var/log/mail.log
```

# Other info related



https://www.digitalocean.com/community/tutorials/how-to-install-and-configure-postfix-on-ubuntu-18-04






### Virtual user (alias account)
sudo postconf -e 'virtual_alias_maps=hash:/etc/postfix/virtual'


sudo nano /etc/postfix/virtual

alumno01@escom.edu usuario

sudo postmap /etc/postfix/virtual 