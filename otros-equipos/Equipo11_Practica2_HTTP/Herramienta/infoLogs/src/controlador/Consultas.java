/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import modelo.Log;

/**
 *
 * @author LeViic
 */
public class Consultas  {
    private final String tabla = "datos";
    
    public List<Log> recuperarTodas(Connection conexion) throws SQLException{
      List<Log> log = new ArrayList<>();
      try{
          
         PreparedStatement consulta = conexion.prepareStatement("SELECT Usuario,Recurso,Estado,Sitio,Puerto FROM datos");
         ResultSet resultado = consulta.executeQuery();
         while(resultado.next()){
            log.add(new Log(resultado.getString("usuario"), resultado.getString("recurso"), resultado.getString("estado"), resultado.getString("sitio"), resultado.getString("puerto")));
         }
      }catch(SQLException ex){
         throw new SQLException(ex);
      }
      return log;
   }
    
    public List<Log> recuperarTrafico(Connection conexion) throws SQLException{
      List<Log> log = new ArrayList<>();
      try{
          
         PreparedStatement consulta = conexion.prepareStatement("SELECT idTrafico,TraficoEntrada,TraficoSalida,TiempoRespuesta FROM trafico");
         ResultSet resultado = consulta.executeQuery();
         while(resultado.next()){
            log.add(new Log(resultado.getString("idTrafico"), resultado.getString("TraficoEntrada"), resultado.getString("TraficoSalida"), resultado.getString("TiempoRespuesta")));
         }
      }catch(SQLException ex){
         throw new SQLException(ex);
      }
      return log;
   }
    
    public List<Log> TiempoPromedio(Connection conexion) throws SQLException{
      int sum;
      int id;
      List<Log> log = new ArrayList<>();
      try{
          
         PreparedStatement consulta = conexion.prepareStatement("SELECT SUM(TiempoRespuesta) / MAX(idTrafico), SUM(TraficoEntrada), SUM(TraficoSalida), MAX(idTrafico) FROM trafico");
         ResultSet resultado = consulta.executeQuery();
         
         while(resultado.next()){
             sum= resultado.getInt(1);
             id= resultado.getInt(2);
             int res = sum/id;
             log.add(new Log(resultado.getString("SUM(TiempoRespuesta) / MAX(idTrafico)"),resultado.getString("SUM(TraficoEntrada)"), resultado.getString("SUM(TraficoSalida)"),resultado.getString("MAX(idTrafico)")));
             System.out.println("Prom: "+res);
         }
         
         
         
      }catch(SQLException ex){
         throw new SQLException(ex);
      }
      return log;
   }
   /*public List<Archive> recuperarUsuario(Connection conexion, String usuario) throws SQLException{
       List<Archive> archives = new ArrayList<>();
       try{
         PreparedStatement consulta = conexion.prepareStatement("SELECT username, timestamp, peer, bare_peer, xml, txt, id, created_at FROM " + this.tabla + " WHERE username = '"+ usuario+"' AND txt IS NOT NULL AND txt<>'' "); 
         ResultSet resultado = consulta.executeQuery();
         while(resultado.next()){
            archives.add(new Archive(resultado.getInt("id"), resultado.getString("username"), resultado.getString("timestamp"), resultado.getString("peer"), resultado.getString("bare_peer"),
                                    resultado.getString("xml"), resultado.getString("txt"), resultado.getString("created_at")));
         }
      }catch(SQLException ex){
         throw new SQLException(ex);
      }
      return archives;
   }
   */
   /*public List<Archive> nombresUsuarios(Connection conexion) throws SQLException{
       List<Archive> archives = new ArrayList<>();
       try{
           PreparedStatement consulta = conexion.prepareStatement("SELECT username FROM " + this.tabla); 
           ResultSet resultado = consulta.executeQuery();
           while(resultado.next()){
               archives.add(new Archive(resultado.getString("username")));
           }  
       }catch(SQLException ex){
           throw new SQLException(ex);
       }
       return archives;
   }*/
   
   public void Insertar(Connection conn) throws SQLException, IOException{
       //0,8,6,7,9
       Log object = new Log();
        String linea;
        
        
        BufferedReader br = new BufferedReader (new FileReader ("access.log"));
        
        while((linea = br.readLine())!=null){
        
        
        System.out.println(linea);
        String datos[] = linea.split(" \\+ ");
        System.out.println("Arreglo: "+Arrays.toString(datos)+", "+datos.length);
        for(int i =0;i<10;i++)
            System.out.println(datos[i]);
            
        //0,8,6,7,9, usuario, recurso, estado, sitio, puerto
        //3-tiempo de respuesta, 4-bytes de entrada, 5 de salida, 6 totales
       
       
        
       int s1 = Integer.parseInt(datos[2]);
       int s2 = Integer.parseInt(datos[3]);
       int s3 = Integer.parseInt(datos[4]);
       
       
       
       System.out.println(datos[0]);
       try{
       PreparedStatement consulta = conn.prepareStatement("INSERT INTO trafico(TiempoRespuesta,TraficoEntrada,TraficoSalida) VALUES('"+s1+"','"+s2+"','"+s3+"');");
       consulta.executeUpdate();
       
       } catch(SQLException ex){
         throw new SQLException(ex);
        }
        }
      
   }
   
  /* public void logTxt(Connection conexion) throws SQLException{
       List<Archive> archives = new ArrayList<>();
       try{
           PreparedStatement consulta = conexion.prepareStatement("SELECT username, timestamp, peer, bare_peer, xml, txt, id, created_at FROM " + this.tabla + " WHERE txt IS NOT NULL AND txt<>'' ");
           ResultSet resultado = consulta.executeQuery();
           String path = "C:\\Users\\LeViic\\Documents\\NetBeansProjects\\xmpp_practica1\\log.txt";
           FileWriter writer;
           
           try{
               File file = new File(path);
               writer = new FileWriter(file,true);
               
               while(resultado.next()){
                   writer.write(resultado.getString("username"));
                   writer.write(",");
                   writer.write(resultado.getString("timestamp"));
                   writer.write(",");
                   writer.write(resultado.getString("peer"));
                   writer.write(",");
                   writer.write(resultado.getString("xml"));
                   writer.write(",");
                   writer.write(resultado.getString("txt"));
                   writer.write(",");
                   writer.write(resultado.getString("id"));
                   writer.write(",");
                   writer.write(resultado.getString("created_at"));
                   writer.write("\r\n");
                   
               }
               writer.close();
               System.out.println("Log finalizado.");
           }catch (IOException e) {
                e.printStackTrace();
            }
           
       
       }catch (SQLException e) {
            e.printStackTrace();
        }
   }*/
}
