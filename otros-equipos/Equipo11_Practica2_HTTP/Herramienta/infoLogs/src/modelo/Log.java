/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import controlador.Consultas;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Arrays;
import java.util.StringTokenizer;
import controlador.Conexion;
import java.sql.Connection;
import java.sql.SQLException;

/**
 *
 * @author LeViic
 */
public class Log {
    
    private String usuario;
    private String recurso;
    private String estado;
    private String sitio;
    private String puerto;
    private String idTrafico,TraficoEntrada,TraficoSalida,TiempoRespuesta, tiempoProm, TraficoEntradaTotal, TraficoSalidaTotal, MaxId;

    public Log(String usuario, String recurso, String estado, String sitio, String puerto) {
        this.usuario = usuario;
        this.recurso = recurso;
        this.estado = estado;
        this.sitio = sitio;
        this.puerto = puerto;
    }

    

    public Log(String tiempoProm, String TraficoEntradaTotal, String TraficoSalidaTotal, String MaxId) {
        this.MaxId = MaxId;
        this.tiempoProm = tiempoProm;
        this.TraficoEntradaTotal = TraficoEntradaTotal;
        this.TraficoSalidaTotal = TraficoSalidaTotal;
    }
    
    
    

    public String getMaxId() {
        return MaxId;
    }

    public void setMaxId(String MaxId) {
        this.MaxId = MaxId;
    }

   
    
    

    public String getTiempoProm() {
        return tiempoProm;
    }

    public String getTraficoEntradaTotal() {
        return TraficoEntradaTotal;
    }

    public String getTraficoSalidaTotal() {
        return TraficoSalidaTotal;
    }

    public void setTiempoProm(String tiempoProm) {
        this.tiempoProm = tiempoProm;
    }

    public void setTraficoEntradaTotal(String TraficoEntradaTotal) {
        this.TraficoEntradaTotal = TraficoEntradaTotal;
    }

    public void setTraficoSalidaTotal(String TraficoSalidaTotal) {
        this.TraficoSalidaTotal = TraficoSalidaTotal;
    }

    

    public String getIdTrafico() {
        return idTrafico;
    }

    public String getTraficoEntrada() {
        return TraficoEntrada;
    }

    public String getTraficoSalida() {
        return TraficoSalida;
    }

    public String getTiempoRespuesta() {
        return TiempoRespuesta;
    }
    
    
    
    public Log(){
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public void setRecurso(String recurso) {
        this.recurso = recurso;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public void setSitio(String sitio) {
        this.sitio = sitio;
    }

    public void setPuerto(String puerto) {
        this.puerto = puerto;
    }

    public void setIdTrafico(String idTrafico) {
        this.idTrafico = idTrafico;
    }

    public void setTraficoEntrada(String TraficoEntrada) {
        this.TraficoEntrada = TraficoEntrada;
    }

    public void setTraficoSalida(String TraficoSalida) {
        this.TraficoSalida = TraficoSalida;
    }

    public void setTiempoRespuesta(String TiempoRespuesta) {
        this.TiempoRespuesta = TiempoRespuesta;
    }
    

    public String getUsuario() {
        return usuario;
    }

    public String getRecurso() {
        return recurso;
    }

    public String getEstado() {
        return estado;
    }

    public String getSitio() {
        return sitio;
    }

    public String getPuerto() {
        return puerto;
    }
    
    
    
}
