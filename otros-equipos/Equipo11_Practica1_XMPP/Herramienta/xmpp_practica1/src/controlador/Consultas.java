/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import modelos.Archive;

/**
 *
 * @author LeViic
 */
public class Consultas {
    
    private final String tabla = "archive";
    
    public List<Archive> recuperarTodas(Connection conexion) throws SQLException{
      List<Archive> archives = new ArrayList<>();
      try{
         PreparedStatement consulta = conexion.prepareStatement("SELECT username, timestamp, peer, bare_peer, xml, txt, id, created_at FROM " + this.tabla + " WHERE txt IS NOT NULL AND txt<>'' ");
         ResultSet resultado = consulta.executeQuery();
         while(resultado.next()){
            archives.add(new Archive(resultado.getInt("id"), resultado.getString("username"), resultado.getString("timestamp"), resultado.getString("peer"), resultado.getString("bare_peer"),
                                    resultado.getString("xml"), resultado.getString("txt"), resultado.getString("created_at")));
         }
      }catch(SQLException ex){
         throw new SQLException(ex);
      }
      return archives;
   }
    
   public List<Archive> recuperarUsuario(Connection conexion, String usuario) throws SQLException{
       List<Archive> archives = new ArrayList<>();
       try{
         PreparedStatement consulta = conexion.prepareStatement("SELECT username, timestamp, peer, bare_peer, xml, txt, id, created_at FROM " + this.tabla + " WHERE username = '"+ usuario+"' AND txt IS NOT NULL AND txt<>'' "); 
         ResultSet resultado = consulta.executeQuery();
         while(resultado.next()){
            archives.add(new Archive(resultado.getInt("id"), resultado.getString("username"), resultado.getString("timestamp"), resultado.getString("peer"), resultado.getString("bare_peer"),
                                    resultado.getString("xml"), resultado.getString("txt"), resultado.getString("created_at")));
         }
      }catch(SQLException ex){
         throw new SQLException(ex);
      }
      return archives;
   }
   
   public List<Archive> nombresUsuarios(Connection conexion) throws SQLException{
       List<Archive> archives = new ArrayList<>();
       try{
           PreparedStatement consulta = conexion.prepareStatement("SELECT username FROM " + this.tabla); 
           ResultSet resultado = consulta.executeQuery();
           while(resultado.next()){
               archives.add(new Archive(resultado.getString("username")));
           }  
       }catch(SQLException ex){
           throw new SQLException(ex);
       }
       return archives;
   }
   
   public void logTxt(Connection conexion) throws SQLException{
       List<Archive> archives = new ArrayList<>();
       try{
           PreparedStatement consulta = conexion.prepareStatement("SELECT username, timestamp, peer, bare_peer, xml, txt, id, created_at FROM " + this.tabla + " WHERE txt IS NOT NULL AND txt<>'' ");
           ResultSet resultado = consulta.executeQuery();
           String path = "C:\\Users\\LeViic\\Documents\\NetBeansProjects\\xmpp_practica1\\log.txt";
           FileWriter writer;
           
           try{
               File file = new File(path);
               writer = new FileWriter(file,true);
               
               while(resultado.next()){
                   writer.write(resultado.getString("username"));
                   writer.write(",");
                   writer.write(resultado.getString("timestamp"));
                   writer.write(",");
                   writer.write(resultado.getString("peer"));
                   writer.write(",");
                   writer.write(resultado.getString("xml"));
                   writer.write(",");
                   writer.write(resultado.getString("txt"));
                   writer.write(",");
                   writer.write(resultado.getString("id"));
                   writer.write(",");
                   writer.write(resultado.getString("created_at"));
                   writer.write("\r\n");
                   
               }
               writer.close();
               System.out.println("Log finalizado.");
           }catch (IOException e) {
                e.printStackTrace();
            }
           
       
       }catch (SQLException e) {
            e.printStackTrace();
        }
   }
}
