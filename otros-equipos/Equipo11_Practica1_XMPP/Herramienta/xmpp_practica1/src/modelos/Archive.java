/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

/**
 *
 * @author LeViic
 */
public class Archive {
    private Integer id;
    private String username;
    private String timestamp;
    private String peer;
    private String bare_peer;
    private String xml;
    private String txt;
    private String created_at;

    public Archive(Integer id, String username, String timestamp, String peer, String bare_peer, String xml, String txt, String created_at) {
        this.id = id;
        this.username = username;
        this.timestamp = timestamp;
        this.peer = peer;
        this.bare_peer = bare_peer;
        this.xml = xml;
        this.txt = txt;
        this.created_at = created_at;
    }
    
    public Archive(String username){
        this.username = username;
    }

    public Archive() {
        this.id = null;
        this.username = null;
        this.timestamp = null;
        this.peer = null;
        this.bare_peer = null;
        this.xml = null;
        this.txt = null;
        this.created_at = null;
    }

    public Integer getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public String getPeer() {
        return peer;
    }

    public String getBare_peer() {
        return bare_peer;
    }

    public String getXml() {
        return xml;
    }

    public String getTxt() {
        return txt;
    }

    public String getCreated_at() {
        return created_at;
    }
    
    
}
