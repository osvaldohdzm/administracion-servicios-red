/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author LeViic
 */
public class InterfazResumen extends javax.swing.JFrame {

    /**
     * Creates new form Interface
     */
    public InterfazResumen() {
        initComponents();
        String cabecera[] = {"Tiempo de transferencia (activo)","Direccion IP","Recurso","Acción","Usuario","Estado"};
        String datos[][]={};
        this.modelo = new DefaultTableModel(datos,cabecera);
        this.modeloVacio = this.modelo;
        this.TableRegistros.setModel(modeloVacio);
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        CheckBoxUsuario = new javax.swing.JCheckBox();
        jLabel16 = new javax.swing.JLabel();
        TextFieldUsername = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        TableRegistros = new javax.swing.JTable();
        ButtonBuscar = new javax.swing.JButton();
        ButtonVerTodo = new javax.swing.JButton();
        CheckBoxRemoteHost = new javax.swing.JCheckBox();
        TextFieldRemoteHost = new javax.swing.JTextField();
        CheckBoxDirection = new javax.swing.JCheckBox();
        ComboBoxDirection = new javax.swing.JComboBox<>();
        ButtonVaciarTabla = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        CheckBoxUsuario.setText("Usuario");
        CheckBoxUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CheckBoxUsuarioActionPerformed(evt);
            }
        });

        jLabel16.setText("Usuario:");

        TextFieldUsername.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldUsername.setEnabled(false);
        TextFieldUsername.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TextFieldUsernameActionPerformed(evt);
            }
        });

        TableRegistros.setFont(new java.awt.Font("sansserif", 0, 10)); // NOI18N
        TableRegistros.setEnabled(false);
        jScrollPane1.setViewportView(TableRegistros);

        ButtonBuscar.setText("Buscar");
        ButtonBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonBuscarActionPerformed(evt);
            }
        });

        ButtonVerTodo.setText("Ver Todo");
        ButtonVerTodo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonVerTodoActionPerformed(evt);
            }
        });

        CheckBoxRemoteHost.setText("Direccion IP");
        CheckBoxRemoteHost.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CheckBoxRemoteHostActionPerformed(evt);
            }
        });

        TextFieldRemoteHost.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        TextFieldRemoteHost.setEnabled(false);

        CheckBoxDirection.setText("Accion");
        CheckBoxDirection.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CheckBoxDirectionActionPerformed(evt);
            }
        });

        ComboBoxDirection.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Descarga (o)", "Carga (i)", "Eliminado (d)" }));
        ComboBoxDirection.setEnabled(false);
        ComboBoxDirection.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ComboBoxDirectionActionPerformed(evt);
            }
        });

        ButtonVaciarTabla.setText("Vaciar Tabla");
        ButtonVaciarTabla.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ButtonVaciarTablaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(CheckBoxRemoteHost, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(TextFieldRemoteHost, javax.swing.GroupLayout.PREFERRED_SIZE, 213, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(CheckBoxDirection, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(ComboBoxDirection, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(layout.createSequentialGroup()
                                    .addGap(6, 6, 6)
                                    .addComponent(ButtonBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addComponent(ButtonVerTodo, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addComponent(ButtonVaciarTabla, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                    .addComponent(CheckBoxUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(TextFieldUsername, javax.swing.GroupLayout.PREFERRED_SIZE, 238, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(297, 297, 297)))
                .addGap(16, 16, 16))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(TextFieldRemoteHost, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(CheckBoxRemoteHost))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(CheckBoxDirection)
                    .addComponent(ComboBoxDirection, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(CheckBoxUsuario)
                    .addComponent(jLabel16)
                    .addComponent(TextFieldUsername, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ButtonBuscar)
                    .addComponent(ButtonVerTodo)
                    .addComponent(ButtonVaciarTabla))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 217, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void CheckBoxUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CheckBoxUsuarioActionPerformed
        // TODO add your handling code here:
        if(this.CheckBoxUsuario.isSelected()){
            this.TextFieldUsername.setEnabled(true);
        } else {
            this.TextFieldUsername.setEnabled(false);
        }
    }//GEN-LAST:event_CheckBoxUsuarioActionPerformed

    private void ButtonBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonBuscarActionPerformed
        // TODO add your handling code here:
        boolean showLine;
        String date;
        File vsftpd = new File("CargasDescargas.log");
        this.TableRegistros.setModel(this.modeloVacio);
        this.modelo = this.modeloVacio;
        vaciarTabla();
        try {
            FileReader fileReader = new FileReader(vsftpd);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String linea;
            int leidos = 0;
            while((linea = bufferedReader.readLine()) != null){
                
                
                String []datos = linea.split(" ");
                showLine = true;
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String mes;
                int numero;
                String signo;
                Date fecha;

// Ip
                if(this.CheckBoxRemoteHost.isSelected()){
                    if(!this.TextFieldRemoteHost.getText().equals(datos[7])){
                        showLine = false;
                    }
                }
// Acción
                if(this.CheckBoxDirection.isSelected()){
                    numero = this.ComboBoxDirection.getSelectedIndex();
                    if(numero == 0 && !datos[12].equals("o")){
                        showLine = false;
                    }
                    if(numero == 1 && !datos[12].equals("i")){
                        showLine = false;
                    }
                    if(numero == 2 && !datos[12].equals("d")){
                        showLine = false;
                    }
                }
// Usuario
                if(this.CheckBoxUsuario.isSelected()){
                    if(!(this.TextFieldUsername.getText().equals(datos[14]))){
                        showLine = false;
                    }
                }

                if(showLine){
                    date = datos[0]+ " "+ datos[3]+ " "+ datos[1]+ " "+ datos[5];
                    Object row[] = {datos[6],datos[7],datos[9],datos[12],datos[14],datos[18]};
                    this.modelo.addRow(row);
                    System.out.println(linea);
                    leidos++;
                }
            }
          
            if(leidos == 0){
                JOptionPane.showMessageDialog(null, "No Hay Coincidencias...");
                System.out.println("NO hay coincidencias");
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(InterfazResumen.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(InterfazResumen.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }//GEN-LAST:event_ButtonBuscarActionPerformed

    private void ButtonVerTodoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonVerTodoActionPerformed
        // TODO add your handling code here:
        String date;
        File vsftpd = new File("CargasDescargas.log");
        this.TableRegistros.setModel(this.modeloVacio);
        this.modelo = this.modeloVacio;
        vaciarTabla();
        try {
            FileReader fileReader = new FileReader(vsftpd);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String linea;
            int leidos = 0;
            while((linea = bufferedReader.readLine()) != null){
                String []datos = linea.split(" ");
                date = datos[0]+ " "+ datos[3]+ " "+ datos[1]+ " "+ datos[5];
                Object row[] = {datos[6],datos[7],datos[9],datos[12],datos[14],datos[18]};
                this.modelo.addRow(row);
                System.out.println(linea);
                leidos++;
            }
         
        } catch (FileNotFoundException ex) {
            Logger.getLogger(InterfazResumen.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(InterfazResumen.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_ButtonVerTodoActionPerformed

    private void CheckBoxDirectionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CheckBoxDirectionActionPerformed
        // TODO add your handling code here:
        if (this.CheckBoxDirection.isSelected()) {
            this.ComboBoxDirection.setEnabled(true);
        } else {
            this.ComboBoxDirection.setEnabled(false);
        }        
    }//GEN-LAST:event_CheckBoxDirectionActionPerformed

    private void CheckBoxRemoteHostActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CheckBoxRemoteHostActionPerformed
        // TODO add your handling code here:
        if (this.CheckBoxRemoteHost.isSelected()) {
            this.TextFieldRemoteHost.setEnabled(true);
        } else {
            this.TextFieldRemoteHost.setEnabled(false);
        }
    }//GEN-LAST:event_CheckBoxRemoteHostActionPerformed

    private void ButtonVaciarTablaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ButtonVaciarTablaActionPerformed
        // TODO add your handling code here:
        vaciarTabla();
        
    }//GEN-LAST:event_ButtonVaciarTablaActionPerformed

    private void TextFieldUsernameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TextFieldUsernameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_TextFieldUsernameActionPerformed

    private void ComboBoxDirectionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ComboBoxDirectionActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ComboBoxDirectionActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(InterfazResumen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(InterfazResumen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(InterfazResumen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(InterfazResumen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                InterfazResumen frame = new InterfazResumen();
                frame.setTitle("Resumen de Operación");
                frame.setVisible(true);
            }
        });
    }
    
    private void vaciarTabla(){
        DefaultTableModel dtm = (DefaultTableModel) this.TableRegistros.getModel();
        while(dtm.getRowCount()>0)dtm.removeRow(0);
    }

    private DefaultTableModel modelo;
    private final DefaultTableModel modeloVacio;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton ButtonBuscar;
    private javax.swing.JButton ButtonVaciarTabla;
    private javax.swing.JButton ButtonVerTodo;
    private javax.swing.JCheckBox CheckBoxDirection;
    private javax.swing.JCheckBox CheckBoxRemoteHost;
    private javax.swing.JCheckBox CheckBoxUsuario;
    private javax.swing.JComboBox<String> ComboBoxDirection;
    private javax.swing.JTable TableRegistros;
    private javax.swing.JTextField TextFieldRemoteHost;
    private javax.swing.JTextField TextFieldUsername;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
