/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dhcp;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 *
 * @author LeViic
 */
public class Dhcp {
 String ip, mac;
 String fechaInicio, fechaFin;
 String horaInicio, horaFin;
 String nombre;
 String tiempoArrendamiento;
 String tiempoMaxArrendamiento;
 String subnetIP, ipI, ipF;
 int rango;
 
    public static void main(String[] args) throws FileNotFoundException, IOException {
        Dhcp d = new Dhcp();
        ArrayList<Dhcp> less = new ArrayList<Dhcp>();
        ArrayList<Dhcp> conf = new ArrayList<Dhcp>();
        less = d.less();
        d.imprimirArreglo(less);
        conf = d.conf();
        d.imprimirSub(conf);
        d.direccionesDisponibles(less, conf);
    }
    
    public void imprimirArreglo(ArrayList<Dhcp> elementos){
        
        System.out.println("Total de direcciones arrendadas: " + elementos.size());
        
        for (int i = 0; i < elementos.size(); i++){
            System.out.println("-------------------------------------------");
            System.out.println("Dirección IP: " + elementos.get(i).ip + "\nDirección MAC: " +
           elementos.get(i).mac + "\nNombre: " + elementos.get(i).nombre + "\nFecha Inicio: " +
           elementos.get(i).fechaInicio + "\nHora Inicio: " + elementos.get(i).horaInicio + "\nFecha Fin: " +
           elementos.get(i).fechaFin + "\nHora Fin: " + elementos.get(i).horaFin);
        }
    }
    
    public void imprimirSub(ArrayList<Dhcp> elementos){
        
        for (int i = 0; i < elementos.size(); i++){
            System.out.println("-------------------------------------------");
            System.out.println("Subred: " + elementos.get(i).subnetIP + "\nDirección Inicio: " +
           elementos.get(i).ipI + "\nDirección Final: " + elementos.get(i).ipF + "\nRango: " +
           elementos.get(i).rango);
        }
    }
    
    public ArrayList<Dhcp> less() throws FileNotFoundException, IOException{
        String s1 = "";
        String s2;
        Dhcp aux = new Dhcp();
        ArrayList<Dhcp> elementos = new ArrayList<Dhcp>();
        BufferedReader br = new BufferedReader(new FileReader("dhcpd.leases"));
        while ((s1 = br.readLine()) != null){
            int numTokens = 0;
            StringTokenizer st = new StringTokenizer(s1);

            while (st.hasMoreTokens()){
                s2 = st.nextToken();
                numTokens++;
                //Dirección
                if (s2.compareTo("lease") == 0){
                    s2 = st.nextToken();
                    s2 = s2.replaceAll(";", "");
                    numTokens++;
                    aux.ip = s2;
                }
                //Inicio
                if (s2.compareTo("starts") == 0){
                    s2 = st.nextToken();
                    numTokens++;
                    s2 = st.nextToken();
                    numTokens++;
                    aux.fechaInicio = s2;
                    s2 = st.nextToken();
                    numTokens++;
                    aux.horaInicio = s2;
                }
                //Fin
                if (s2.compareTo("ends") == 0){
                    s2 = st.nextToken();
                    numTokens++;
                    s2 = st.nextToken();
                    numTokens++;
                    aux.fechaFin = s2;
                    s2 = st.nextToken();
                    numTokens++;
                    aux.horaFin = s2;
                }
                //Nombre
                if ((s2.compareTo("client-hostname")) == 0 || (s2.compareTo("set vendor-class-identifier ="))== 0){
                    s2 = st.nextToken();
                    numTokens++;
                    aux.nombre
                    = s2;
                    elementos.add(aux);
                    aux = new Dhcp();
                }
                //MAC
                if (s2.compareTo("hardware") == 0){
                    s2 = st.nextToken();
                    numTokens++;
                    s2 = st.nextToken();
                    numTokens++;
                    aux.mac = s2;
                }
            }
        }
        return elementos;
       }
    
    public ArrayList<Dhcp> conf() throws FileNotFoundException, IOException{
       String s1 = "";
       String s2, auxS = "";
       Dhcp aux = new Dhcp();
       ArrayList<Dhcp> elementos = new ArrayList<Dhcp>();
       BufferedReader br = new BufferedReader(new FileReader("dhcpd.conf"));

        while ((s1 = br.readLine())!= null){
            int numTokens = 0, estado= -1;
            StringTokenizer st = new StringTokenizer(s1);

            while (st.hasMoreTokens()){
                s2 = st.nextToken();
                numTokens++;
                //Dirección
                if (s2.compareTo("default-lease-time") == 0){
                    s2 = st.nextToken();
                    numTokens++;
                    tiempoArrendamiento = s2;
                    System.out.println("-------------------------------------------");
                    System.out.println("Tiempo de arrendamiento: " + tiempoArrendamiento);
                }
                if (s2.compareTo("max-lease-time") == 0){
                    s2 = st.nextToken();
                    numTokens++;
                    tiempoMaxArrendamiento = s2;
                    System.out.println("Tiempo de máximo de arrendamiento: " + tiempoMaxArrendamiento);
                }
                //Dirección subred
                if (s2.compareTo("subnet") == 0){
                    s2 = st.nextToken();
                    numTokens++;
                    aux.subnetIP= s2;
                    auxS = s2;
                    estado = 0;
                }
                //Rangos subred
                if (s2.compareTo("range") == 0){
                    if (estado == 0){
                        s2 = st.nextToken();
                        numTokens++;
                        s2 = s2.replaceAll(";", "");
                        aux.ipI = s2;
                        s2 = st.nextToken();
                        numTokens++;
                        s2 = s2.replaceAll(";", "");
                        aux.ipF = s2;
                        aux.rango = Integer.parseInt(direccionIP(aux.ipF))-
                       Integer.parseInt(direccionIP(aux.ipI));
                        estado= -1;
                        elementos.add(aux);
                        aux = new Dhcp();
                    }else{
                        aux.subnetIP = auxS;
                        s2 = st.nextToken();
                        numTokens++;
                        s2 = s2.replaceAll(";", "");
                        aux.ipI = s2;
                        s2 = st.nextToken();
                        numTokens++;
                        s2 = s2.replaceAll(";", "");
                        aux.ipF = s2;
                        aux.rango = Integer.parseInt(direccionIP(aux.ipF)) -
                       Integer.parseInt(direccionIP(aux.ipI));
                        elementos.add(aux);
                        aux = new Dhcp();
                    }
                }
            }
        }
        return elementos;
    }
    
    public String direccionIP(String direccion){
        String aux = "";
        for (int i = 0; i < direccion.length(); i++){
            if (direccion.charAt(i) != '.'){
                aux = aux + direccion.charAt(i);
            }
                else{
                    aux = "";
                }
        }
    return aux;
    }
    
    public String inicioCadena(String direccion){
        String aux = "";
        int puntos = 0;
    
        for (int i = 0; i < direccion.length(); i++){
            if (puntos <= 2){
                if (direccion.charAt(i) != '.'){
                    aux = aux + direccion.charAt(i);
                }else{
                    aux = aux + direccion.charAt(i);
                    puntos++;
                }
            }
        }
        aux = aux.substring(0, aux.length() - 1);
    return aux;
    }
    
    public void direccionesDisponibles(ArrayList<Dhcp> less, ArrayList<Dhcp> conf){
        System.out.println("-------------------------------------------");
        String inicio;
        int totales;
        
        int ocupadas = 0, men, may, asig;
        
        for (int i = 0; i < conf.size(); i++){
            inicio = inicioCadena(conf.get(i).subnetIP);
            men = Integer.parseInt(direccionIP(conf.get(i).ipI));
            may = Integer.parseInt(direccionIP(conf.get(i).ipF));
            
            for (int j = 0; j < less.size(); j++){
                asig = Integer.parseInt(direccionIP(less.get(j).ip));
                if (less.get(j).ip.startsWith(inicio) && asig <= may && asig >= men){
                    ocupadas++;
                }
            }
            totales = ocupadas + conf.get(i).rango;
        System.out.println("La subred que inicia en: " + conf.get(i).ipI + " y termina en: " + conf.get(i).ipF +  "\n Cuenta con " + ocupadas + 
                " ip's ocupadas de " + conf.get(i).rango + " ip's disponibles." + "\n Cuenta con " + totales + "ip´s para asignar" + "\n-------------------------------------------------------");
        ocupadas = 0;
        }
    }
}
