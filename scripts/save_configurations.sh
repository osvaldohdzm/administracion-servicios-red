rm -r /home/usuario/Escritorio/administracion-servicios-red/configuration-files/*

DIR="/home/usuario/Escritorio/administracion-servicios-red/configuration-files/sites-available"
if [ -d "$DIR" ]; then
  ### Take action if $DIR exists ###	 
  cp -r /etc/nginx/sites-available/*  /home/usuario/Escritorio/administracion-servicios-red/configuration-files/sites-available/
else
  ###  Control will jump here if $DIR does NOT exists ###
  cp -r /etc/nginx/sites-available/  /home/usuario/Escritorio/administracion-servicios-red/configuration-files/
fi


DIR="/home/usuario/Escritorio/administracion-servicios-red/configuration-files/html"
if [ -d "$DIR" ]; then
  ### Take action if $DIR exists ###
	cp -r /var/www/html/*  /home/usuario/Escritorio/administracion-servicios-red/configuration-files/html/ 
else
  ###  Control will jump here if $DIR does NOT exists ###
	cp -r /var/www/html/  /home/usuario/Escritorio/administracion-servicios-red/configuration-files/ 
fi

cp /etc/nginx/nginx.conf /home/usuario/Escritorio/administracion-servicios-red/configuration-files/

chmod 777 -R /home/usuario/Escritorio/administracion-servicios-red/


