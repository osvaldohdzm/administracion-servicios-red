import re

def lectura():
    
    print("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n")
    print("--- TABLA DE PETICIONES RECIBIDAS ---")
    #listaLineas = []
    filename = "/var/log/nginx/dev_access.log"
    #filename = "/home/alex/Desktop/dev_access.log"
    with open(filename) as f:

        textoLogs = f.read()

        #listaLineas = [line.rstrip('\n') for line in f]
        #print(listaLineas[1])
        
        rfecha = r"(\d{2}/\w{3}/\d{4}:\d{1,2}:\d{1,2}:\d{1,2})"
        rIP = r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}|::1'
        rRecurso = r"\"(.*?)\""
        rRespuesta = r"\" \d{1,3}"

        listaFechas = re.findall(rfecha, textoLogs)
        listaIP = re.findall(rIP, textoLogs)
        listaRecurso = re.findall(rRecurso, textoLogs)
        listaRespuesta = re.findall(rRespuesta, textoLogs)

        requestos = len(listaFechas)
        for i in range(0,requestos):
            print("IP: {}\tFecha: {}\tRecurso: {}\tCodigo HTTP: {}".format(listaIP[i],listaFechas[i],listaRecurso[i],listaRespuesta[i]))

def lecturaerrores():
    print("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n")
    print("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n")
    print("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n")
    print("--- TABLA DE ERRORES ---")
    #listaLineas = []
    filename = "/var/log/nginx/dev_access.log"
    #filename = "/home/alex/Desktop/dev_access.log"
    with open(filename) as f:

        textoLogs = f.read()

        #listaLineas = [line.rstrip('\n') for line in f]
        #print(listaLineas[1])
        
        rfecha = r"(\d{2}/\w{3}/\d{4}:\d{1,2}:\d{1,2}:\d{1,2})"
        rIP = r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}|::1'
        rRecurso = r"\"(.*?)\""
        rError = r"\" 4\d{1,2}"


        listaFechas = re.findall(rfecha, textoLogs)
        listaIP = re.findall(rIP, textoLogs)
        listaRecurso = re.findall(rRecurso, textoLogs)
        listaError = re.findall(rError, textoLogs)

        requestos = len(listaError)

        for i in range(0,requestos):
                print("IP: {}\tFecha: {}\tRecurso: {}\tCodigo HTTP: {}".format(listaIP[i],listaFechas[i],listaRecurso[i],listaError[i]))
    


if __name__ == "__main__":
    lectura()
    lecturaerrores()

